import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import JsonExcel from 'vue-json-excel';
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
Vue.config.productionTip = false;
Vue.component('downloadExcel', JsonExcel);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
