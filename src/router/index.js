import Vue from "vue";
import VueRouter from "vue-router";
import AssetInvent from "../views/AssetInventory.vue";
import AssetDetail from "../views/AssetDetail.vue";
import Policies from "../views/Policies.vue";
import Overview from "../views/Overview.vue";
import Report from "../views/Report.vue";
import Account from "../views/Account.vue";
import Alert from "../views/Alert.vue";
import Investigate from "../views/Investigate.vue";
import AlertRules from "../views/AlertRules.vue";
import NotificationTemp from "../views/NotificationTemp.vue";
import OverviewDetail from "../views/OverviewDetail.vue";
import NewPolicy from "../views/NewPolicy.vue";
import NewAccount from "../views/NewAccount.vue";
import NewReport from "../views/NewReport.vue";
import NewAlertRule from "../views/NewAlertRule.vue";
import AddNotifyTemp from "../views/AddNotifyTemp.vue";
import AlertDetail from "../views/AlertDetail.vue";
Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/dashboard/asset_inventory"
    },
    {
      path: "/dashboard/asset_inventory",
      name: "asset_inventory",
      component: AssetInvent
    },
    {
      path: "/dashboard/asset_detail/:id",
      name: "asset_detail",
      component: AssetDetail
    },
    {
      path: "/compliance/overview/:id",
      name: "overview_detail",
      component: OverviewDetail
    },
    {
      path: "/policies",
      name: "policies",
      component: Policies
    },
    {
      path: "/policies/new",
      name: "new_policy",
      component: NewPolicy
    },
    {
      path: "/compliance/overview",
      name: "overview",
      component: Overview
    },
    {
      path: "/compliance/reports",
      name: "report",
      component: Report
    },
    {
      path: "/compliance/reports/new",
      name: "new_report",
      component: NewReport
    },
    {
      path: "/account",
      name: "account",
      component: Account
    },
    {
      path: "/account/new",
      name: "new_account",
      component: NewAccount
    },
    {
      path: "/alerts/overview",
      name: "alert",
      component: Alert
    },
    {
      path: "/alerts/overview/detail",
      name: "alert_detail",
      component: AlertDetail
    },
    {
      path: "/alerts/alert_rules",
      name: "alert_rules",
      component: AlertRules
    },
    {
      path: "/alerts/alert_rules/new",
      name: "new_alert_rules",
      component: NewAlertRule
    },
    {
      path: "/alerts/notification_templates",
      name: "alert",
      component: NotificationTemp
    },
    {
      path: "/alerts/notification_templates/new",
      name: "add_alert_notify_temp",
      component: AddNotifyTemp
    },
    {
      path: "/investigate/:id",
      name: "investigate",
      component: Investigate
    }
  ]
});

export default router;
