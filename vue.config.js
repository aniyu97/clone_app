module.exports = {
  configureWebpack: config => {
    config.resolve.extensions.push(".some.new.extension");
    // instead of config.resolve.extensions = ['.some.new.extension']
  },
  transpileDependencies: ["vuetify"]
};
